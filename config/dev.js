const baseApi = "https://gatewaytest.sztaizhou.com";
const isH5 = process.env.TARO_ENV === "h5";

module.exports = {
  env: {
    NODE_ENV: '"development"',
  },
  defineConstants: {
    ENV_BASE_API: isH5 ? '"/api"' : JSON.stringify(baseApi),
  },
  mini: {},
  h5: {
    devServer: {
      proxy: {
        "/api": {
          target: baseApi,
          pathRewrite: {
            "^/api": "",
          },
          changeOrigin: true,
        },
      },
    },
  },
};
