import path from "path";
let baseApi = "https://gatewaytest.sztaizhou.com";

//prod指令控制 链接后台xcx正式服务器
if (process.argv.includes("api_prod")) {
  baseApi = "https://xcx.sztaizhou.com";
}
console.log("\x1B[31m%s\x1B[0m", "------- 本次打包后台地址 ---------------");
console.log("\x1B[36m%s\x1B[0m", "-------- " + baseApi + "---------------");

const isH5 = process.env.TARO_ENV === "h5";

module.exports = {
  env: {
    NODE_ENV: '"production"',
  },
  defineConstants: {
    ENV_BASE_API: isH5 ? '"/api"' : JSON.stringify(baseApi),
  },
  mini: {
    commonChunks: ["runtime", "vendors", "taro", "common", "nutui"],
    webpackChain(chain) {
      chain.merge({
        optimization: {
          splitChunks: {
            cacheGroups: {
              nutui: {
                name: "nutui",
                test: /[\\/]node_modules[\\/]@nutui[\\/]/,
                priority: 3,
                reuseExistingChunk: true,
              },
            },
          },
        },
      });
      chain.plugin("providerPlugin").tap((args) => {
        args[0].$TzNotify = ["@/components/tzUI/TzNotify/index", "TzNotify"];
        return args;
      });
      /* chain
          .plugin("analyzer")
          .use(require("webpack-bundle-analyzer").BundleAnalyzerPlugin, []); */
    },
  },
  h5: {
    //如果h5端编译后体积过大，可以使用webpack-bundle-analyzer插件对打包体积进行分析。
    // 参考代码如下：
    webpackChain(chain, webpack) {
      chain.plugin("providerPlugin").use(webpack.ProvidePlugin, [
        {
          $TzNotify: [
            path.resolve(__dirname, "..", "src/components/tzUI/TzNotify/index"),
            "TzNotify",
          ],
        },
      ]);
      /*     chain.merge({
        optimization: {
          splitChunks: {
            cacheGroups: {
              nutui: {
                name: "nutui",
                test: /[\\/]node_modules[\\/]@nutui[\\/]/,
                priority: 3,
                reuseExistingChunk: true,
              },
            },
          },
        },
      }),
        chain
          .plugin("analyzer")
          .use(require("webpack-bundle-analyzer").BundleAnalyzerPlugin, []); */
    },
  },
};
