import path from "path";
console.log(path.resolve(__dirname, "..", "src/"));
const config = {
  projectName: "tz-weApp-tcs",
  date: "2021-8-29",
  designWidth: 375,
  deviceRatio: {
    640: 2.34 / 2,
    750: 1,
    828: 1.81 / 2,
    375: 2 / 1,
  },
  sourceRoot: "src",
  outputRoot: "dist",
  plugins: ["@tarojs/plugin-html"],
  defineConstants: {},
  copy: {
    patterns: [],
    options: {},
  },
  alias: {
    "@": path.resolve(__dirname, "..", "src"),
  },
  sass: {
    resource: [path.resolve(__dirname, "..", "src/lib/nutui/variables.scss")],
  },
  framework: "vue3",
  mini: {
    postcss: {
      pxtransform: {
        enable: true,
        config: {},
      },
      url: {
        enable: true,
        config: {
          limit: 1024, // 设定转换尺寸上限
        },
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: "module", // 转换模式，取值为 global/module
          generateScopedName: "[name]__[local]___[hash:base64:5]",
        },
      },
    },
    // 自定义 Webpack 配置
    webpackChain(chain) {
      chain.plugin("providerPlugin").tap((args) => {
        args[0].$TzNotify = ["@/components/tzUI/TzNotify/index", "TzNotify"];
        return args;
      });

      /*    chain.plugin("providerPlugin").use(webpack.ProvidePlugin, [
        {
          $Taro: "@tarojs/taro",
        },
      ]); */
    },
  },
  h5: {
    publicPath: "/",
    staticDirectory: "static",
    postcss: {
      autoprefixer: {
        enable: true,
        config: {},
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: "module", // 转换模式，取值为 global/module
          generateScopedName: "[name]__[local]___[hash:base64:5]",
        },
      },
    },
    vue: {},
    router: {
      mode: "browser", // 或者是 'browser,hash'
    },
    // 自定义 Webpack 配置
    webpackChain(chain, webpack) {
      chain.plugin("providerPlugin").use(webpack.ProvidePlugin, [
        {
          $TzNotify: [
            path.resolve(__dirname, "..", "src/components/tzUI/TzNotify/index"),
            "TzNotify",
          ],
        },
      ]);
    },
  },
};

module.exports = function(merge) {
  if (process.env.NODE_ENV === "development") {
    return merge({}, config, require("./dev"));
  }
  return merge({}, config, require("./prod"));
};
