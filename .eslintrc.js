module.exports = {
  root: true,
  env: {
    node: true,
    es6: true,
  },
  globals: {
    $ztStore: true,
    $Taro: true,
    $TzNotify: true,
    ENV_BASE_API: true,
  },
  extends: ["plugin:vue/vue3-recommended"],
  // 0关闭  1警告 2 error
  rules: {
    "no-var": 1, //不使用var
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off", //生产环境不能用 console.log()
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off", // 生产环境不能有 debugger
    "no-unused-vars": "warn", //定义了但未使用得变量，warn警告
    "no-empty": "warn", //块语句中的内容不能为空,warn警告
    "no-undef": "error", //变量未声明就使用
    "no-compare-neg-zero": 2, //禁止与 -0 进行比较
    "no-alert": 0, //禁止使用alert confirm prompt
    "no-array-constructor": 2, //禁止使用数组构造器
    "no-duplicate-case": 2, //switch中的case标签不能重复
    "no-empty-character-class": 2, //正则表达式中的[]内容不能为空
    "no-eq-null": 2, //禁止对null使用==或!=运算符
    "no-eval": 1, //禁止使用eval
    "no-extra-semi": 2, //禁止多余的冒号
    "no-fallthrough": 1, //禁止switch穿透
    "no-constant-condition": 2, //禁止在条件中使用常量表达式 if(true) if(1)
    "no-delete-var": 2, //不能对var声明的变量使用delete操作符
    "no-ex-assign": 2, //禁止给catch语句中的异常参数赋值
    "no-iterator": 2, //禁止使用__iterator__ 属性
    "no-label-var": 2, //label名不能与var声明的变量名相同
    "no-labels": 2, //禁止标签声明
    "no-lone-blocks": 1, //禁止不必要的嵌套块
    "linebreak-style": [0, "windows"], //换行风格
    "no-with": 2, //禁用with
    "use-isnan": 2, //禁止比较时使用NaN，只能用isNaN()
    "valid-jsdoc": 0, //jsdoc规则
    "valid-typeof": 2, //必须使用合法的typeof的值
    // "vars-on-top": 2,//var必须放在作用域顶部
    "wrap-iife": [2, "inside"], //立即执行函数表达式的小括号风格
    "wrap-regex": 0, //正则表达式字面量用小括号包起来
    yoda: [2, "never"], //禁止尤达条件
    "consistent-this": [2, "that"], //this别名
    "default-case": 2, //switch语句最后必须有default
    "dot-location": 0, //对象访问符的位置，换行的时候在行首还是行尾
    "dot-notation": [0, { allowKeywords: true }], //避免不必要的方括号
    "eol-last": 0, //文件以单一的换行符结束
    eqeqeq: 2, //必须使用全等
    "prefer-const": 1, //授权const
    "prefer-spread": 0, //首选展开运算
    "prefer-reflect": 0, //首选Reflect的方法
    semi: [2, "always"], //语句强制分号结尾
    indent: 0, //缩进

    "vue/no-side-effects-in-computed-properties": "off",
    "vue/singleline-html-element-content-newline": "off",
    "vue/max-attributes-per-line": [
      "warn",
      {
        singleline: 4,
        multiline: {
          max: 4,
          allowFirstLine: true,
        },
      },
    ],
    "vue/html-indent": 0,
    "vue/html-self-closing": 0,
    "vue/component-name-in-template-casing": [
      "error",
      "kebab-case",
      {
        registeredComponentsOnly: false,
        ignores: [],
      },
    ],
    "vue/multiline-html-element-content-newline": [
      "warn",
      {
        ignoreWhenEmpty: true,
        ignores: ["VueComponent", "pre", "textarea"],
        allowEmptyLines: true,
      },
    ],

    //camelcase: 1, //强制驼峰命名
  },
  plugins: ["vue"],
  parserOptions: {
    parser: "babel-eslint",
  },
};
