import { Toast, Icon } from "@nutui/nutui-taro";
import "@nutui/nutui-taro/dist/styles/themes/default.scss";

const nutui = {
  install: function (app) {
    app.use(Toast).use(Icon);
  },
};

export default nutui;
