/**
 * axios二次封装,处理了一些公共参数,token,key加密相关处理
 * 可根据自己个人习惯,删除修改
 */

import Taro from "@tarojs/taro";
import "./interceptor";
import config from "./config";
import { copyObject, errHandler } from "./util";

/**
 * 参数转化,主要处理公共参数,md5等验签加密
 * @param url  url
 * @param params 加密前的对象
 * @returns {string} 加密后的数据
 */
const paramsConvert = (params) => {
  return copyObject(params, config.defaultParams());
};

/**
 * httpClient发送axios请求
 * @param {Object} option  //参数
 */
export const httpClient = async (option) => {
  option.data = paramsConvert(option.data);
  option.header = copyObject(option.header, config.defaultHeaders());
  option.timeout = option.timeout || "8000";
  const pattern = /^http[s]?:\/\/.+/;
  if (!pattern.test(option.url)) {
    option.url = ENV_BASE_API + option.url;
  }
  option.fail = function(err) {
    console.log(err);
    errHandler(err);
  };
  return Taro.request(option);
};

//这里只是初步封装,可以自行抽离
export default {
  /**
   * get请求,参数默认在query中,用?拼接
   * @param {String} url url
   * @param {Object} params 参数
   * @param {*} argType 参数拼接方式 （TYPE_BODY即参数在Body中）
   * @returns  Promise
   */
  get: (option) => {
    option["method"] = "GET";
    return httpClient(option);
  },

  /**
   * delete请求,参数默认在query中,用?拼接
   * @param {String} url url
   * @param {Object} params 参数
   * @param {*} argType 参数拼接方式 TYPE_BODY参数在Body中
   * @returns  Promise
   */
  delete: (option) => {
    option["method"] = "DELETE";
    return httpClient(option);
  },

  /**
   * post请求
   * @param {Object} option {url,headers,data,params}
   * @returns  Promise
   */
  post: (option) => {
    option["method"] = "POST";
    return httpClient(option);
  },

  /**
   * put请求,参数默认在body中
   * @param {String} url url
   * @param {Object} params 参数
   * @param {*} argType 参数拼接方式 TYPE_URL即参数在URL后面
   * @returns  Promise
   */
  put: (option) => {
    option["method"] = "PUT";
    return httpClient(option);
  },

  /**
   * 文件上传默认使用表单上传
   * @param {string} url  api路由
   * @param {Object} params 参数
   */
  upload: (url, params) => {
    const param = new FormData();
    // eslint-disable-next-line no-unused-vars
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        param.append(key, params[key]);
      }
    }
    return httpClient({
      method: "POST",
      header: {
        "Content-Type": "multipart/form-data",
      },
      url: config.baseUrl + url,
      data: param,
    });
  },
};
