import Taro from "@tarojs/taro";
import { getToken } from "@/utils/storage";
import { errHandler } from "./util";

export const interceptor = function (chain) {
  const reqParams = chain.requestParams;
  const token = getToken();
  //是否有局部的basicToken
  const basicToken = reqParams.header["Authorization"];
  if (token && !basicToken) {
    reqParams.header["Authorization"] = token;
  }
  //如果为none就不加Authorization
  if (basicToken === "none") {
    delete reqParams.header.Authorization;
  }
  return chain.proceed(reqParams).then((res) => {
    const data = res.data;
    if (res.statusCode === 200) {
      if (data.code && data.code !== 0) {
        return Promise.reject(res);
      } else {
        return data;
      }
    } else {
      errHandler(res);
      return Promise.reject(res);
    }
  });
};
Taro.addInterceptor(interceptor);
