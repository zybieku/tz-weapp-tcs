const md5Params = {
  md5key: "TJTXOswvftDc7zNc",
  signKey: "sign",
};

const defaultParams = () => {
  return null;
};
const defaultHeaders = () => {
  return {
    apiver: "v1.2.0",
  };
};

export default {
  md5Params,
  defaultParams,
  defaultHeaders,
};
