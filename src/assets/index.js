/**
 * 图片统一管理，方便全局替换
 */
export { default as icLogo } from "@/assets/images/ic_logo.png";

export { default as bgLoginHeader } from "@/assets/images/bg_login_header.jpg";

//大陆段图标
export { default as icCnWgkckb } from "@/assets/images/ic_cn_wgkckb.png";
export { default as icCnYgkckb } from "@/assets/images/ic_cn_ygkckb.png";
export { default as icCnZckb } from "@/assets/images/ic_cn_zckb.png";
export { default as icCnQbcx } from "@/assets/images/ic_cn_qbcx.png";
export { default as icCnClcx } from "@/assets/images/ic_cn_clcx.png";
export { default as icCnJb } from "@/assets/images/ic_cn_jb.png";

//香港段图标
export { default as icHkWf } from "@/assets/images/ic_hk_wf.png";
export { default as icHkWfcx } from "@/assets/images/ic_hk_wfcx.png";
export { default as icHkKb } from "@/assets/images/ic_hk_kb.png";
export { default as icHkKbcx } from "@/assets/images/ic_hk_kbcx.png";
export { default as icHkjb } from "@/assets/images/ic_hk_jb.png";
