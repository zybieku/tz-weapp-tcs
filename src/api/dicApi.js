/**
 * 加载字典
 */
export function loadDicMap(dicMap) {
  dicMap.statusDic = {
    //确报状态
    "00": "草稿",
    "03": "重新编辑",
    "10": "待申报",
    "23": "待海关人工审核",
    "51": "解绑成功",
    "52": "解绑失败",
    "31": "捆绑成功",
    "32": "捆绑失败",
    "30": "捆绑中",
    "60": "退单重报",
    "61": "退单重删",
    "50": "正在解绑",
  };
  dicMap.statusDicList = [
    //确报状态
    { para_value: "", para_name: "--请选择--" },
    { para_value: "00", para_name: "草稿" },
    { para_value: "03", para_name: "重新编辑" },
    { para_value: "10", para_name: "待申报" },
    { para_value: "23", para_name: "待海关人工审核" },
    { para_value: "51", para_name: "解绑成功" },
    { para_value: "52", para_name: "解绑失败" },
    { para_value: "31", para_name: "捆绑成功" },
    { para_value: "32", para_name: "捆绑失败" },
    { para_value: "30", para_name: "捆绑中" },
    { para_value: "60", para_name: "退单重报" },
    { para_value: "61", para_name: "退单重删" },
    { para_value: "50", para_name: "正在解绑" },
  ];
  dicMap.inOutTypeDic = {
    //确报单据类型
    MT4401: "进境(重车确报)",
    MT4402: "出境(重车确报)",
    MT4403: "进境(空载确报)",
    MT4404: "出境(空载确报)",
    MT4405: "进境(搭载空箱确报)",
    MT4406: "出境(搭载空箱确报)",
  };
  dicMap.inOutTypeDicList = [
    //确报单据类型
    { para_value: "", para_name: "--请选择--" },
    { para_value: "MT4401", para_name: "进境(重车确报)" },
    { para_value: "MT4402", para_name: "出境(重车确报)" },
    { para_value: "MT4403", para_name: "进境(空载确报)" },
    { para_value: "MT4404", para_name: "出境(空载确报)" },
    { para_value: "MT4405", para_name: "进境(搭载空箱确报)" },
    { para_value: "MT4406", para_name: "出境(搭载空箱确报)" },
  ];
  dicMap.goodsCustomsCode = {
    //货物通关代码
    RD11: "RD11公路暂时进出境空集装箱",
    RD12: "RD12公路暂时进境空集装箱口岸卸箱",
  };
  dicMap.goodsCustomsCodeList = [
    //货物通关代码
    { para_value: "", para_name: "--请选择--" },
    { para_value: "RD11", para_name: "RD11公路暂时进出境空集装箱" },
    { para_value: "RD12", para_name: "RD12公路暂时进境空集装箱口岸卸箱" },
  ];
  dicMap.boxingSealedType = {
    //封志类型
    M: "M 机械封志",
    E: "E 电子封志",
  };
  dicMap.boxingSealedTypeList = [
    //封志类型
    { para_value: "", para_name: "--请选择--" },
    { para_value: "M", para_name: "M 机械封志" },
    { para_value: "E", para_name: "E 电子封志" },
  ];
  dicMap.boxingSealedName = {
    //施封人
    AA: "AA 拼箱人",
    AB: "AB 未知",
    AC: "AC 检疫",
    CA: "CA 承运人",
    CU: "CU 海关",
    SH: "SH 发货人",
    TO: "TO 码头",
  };
  dicMap.boxingSealedNameList = [
    //施封人
    { para_value: "", para_name: "--请选择--" },
    { para_value: "AA", para_name: "AA 拼箱人" },
    { para_value: "AB", para_name: "AB 未知" },
    { para_value: "AC", para_name: "AC 检疫" },
    { para_value: "CA", para_name: "CA 承运人" },
    { para_value: "CU", para_name: "CU 海关" },
    { para_value: "SH", para_name: "SH 发货人" },
    { para_value: "TO", para_name: "TO 码头" },
  ];

  dicMap.billStatusDic = {
    //确报状态
    DRAFT: "草稿",
    READYTOSEND: "准备寄出",
    SENDING: "寄出中",
    GOVTRECEIVED: "捆绑成功",
    CROSSED: "已过境",
    ISUNBUNDLING: "正在解绑",
    UNBUNDLINGSUCCESS: "解绑成功",
    UNBUNDLINGFAILURE: "解绑失败",
    ERROR: "错误",
    UNKNOWN: "未知状态",
  };

  dicMap.billStatusList = [
    //捆绑单据状态
    { para_value: "", para_name: "全部" },
    { para_value: "DRAFT", para_name: "草稿" },
    { para_value: "READYTOSEND", para_name: "准备寄出" },
    { para_value: "SENDING", para_name: "寄出中" },
    { para_value: "GOVTRECEIVED", para_name: "捆绑成功" },
    { para_value: "CROSSED", para_name: "已过境" },
    { para_value: "ISUNBUNDLING", para_name: "正在解绑" },
    { para_value: "UNBUNDLINGSUCCESS", para_name: "解绑成功" },
    { para_value: "UNBUNDLINGFAILURE", para_name: "解绑失败" },
    { para_value: "ERROR", para_name: "错误" },
    { para_value: "UNKNOWN", para_name: "未知状态" },
  ];

  dicMap.ieTypeDic = {
    //进出口类型
    "": "",
    I: "大陆 → 香港",
    E: "香港 → 大陆",
  };
  dicMap.ieTypeList = [
    //进出口类型
    {
      para_value: "",
      para_name: "--请选择--",
    },
    {
      para_value: "I",
      para_name: "大陆 → 香港",
    },
    { para_value: "E", para_name: "香港 → 大陆" },
  ];

  dicMap.passControlDic = {
    //过关管制站
    "": "",
    LMC: "落马洲",
    MKT: "文锦渡",
    STK: "沙头角",
    SBC: "深圳湾",
    HBM: "港珠澳大桥(来自/到澳门)",
    HBZ: "港珠澳大桥(来自/到珠海)",
    HYW: "香园围",
  };
  dicMap.passControlList = [
    //过关管制站
    { para_value: "", para_name: "--请选择--" },
    { para_value: "LMC", para_name: "落马洲" },
    { para_value: "MKT", para_name: "文锦渡" },
    { para_value: "STK", para_name: "沙头角" },
    { para_value: "SBC", para_name: "深圳湾" },
    { para_value: "HBM", para_name: "港珠澳大桥(来自/到澳门)" },
    { para_value: "HBZ", para_name: "港珠澳大桥(来自/到珠海)" },
    { para_value: "HYW", para_name: "香园围" },
  ];
  dicMap.modeOfTransportList = [
    // 香港捆绑-运输方式
    { code: "1", nameCn: "海运货物" },
    { code: "4", nameCn: "空运货物" },
  ];

  const defaultItem = {
    code: "",
    nameCn: "--请选择--",
    nameLabel: "--请选择--",
  };

  //国家地区
  dicMap.stateCodeList?.map((item) => {
    item.nameLabel = `${item.code}(${item.nameCn})`;
    return item;
  });
  dicMap.stateCodeList.unshift(defaultItem);

  // 包装种类
  dicMap.definiteTypeList.unshift(defaultItem);

  //packTypeList
  dicMap.packTypeList?.map((item) => {
    item.nameLabel = `${item.code}(${item.nameCn})`;
    return item;
  });
  dicMap.packTypeList.unshift(defaultItem);

  //体积单位
  dicMap.customsVolList?.map((item) => {
    item.nameLabel = `${item.code}(${item.nameCn})`;
    return item;
  });
  dicMap.customsVolList.unshift(defaultItem);

  //毛重单位
  dicMap.customsWgtList?.map((item) => {
    item.nameLabel = `${item.code}(${item.nameCn})`;
    return item;
  });
  dicMap.customsWgtList.unshift(defaultItem);
}
