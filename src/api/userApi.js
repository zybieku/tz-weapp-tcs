import api from "@/lib/tzAxios";
/**
 * 获取token
 * @param {*} data
 * @returns
 */
export const login = (data) => {
  const option = {
    url: "/wx/login",
    data,
  };
  return api.post(option);
};

/**
 * 获取用户信息基础数据
 * @param {*} data
 * @returns
 */
export const getBaseData = () => {
  const option = {
    url: "/wx/getBaseData",
  };
  return api.post(option);
};

/**
 * 获取用户信息
 */
export const getUserinfoApi = () => {
  const option = {
    url: "/wx/getUserinfo",
  };
  return api.post(option);
};

/**
 * 更新 token 接口
 */
export const refreshTokenApi = (data) => {
  const option = {
    url: "/wx/refreshToken",
    data,
  };
  return api.post(option);
};
