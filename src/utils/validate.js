/**
 * 是否空字符串
 * @param {*} str
 * @returns Boolean
 */
export const isEmpty = (str) => {
  // REVIEW: 后续调整这个方法
  if (typeof str === "number") {
    str = str.toString();
  }
  return (
    str === null || str === undefined || str.trim() === "" || str === "null"
  );
};

/**
 * 配置普通常用字符
 * 中文、数字、字母、常用符号：`~!@#$%^/_-
 * @param {*} str
 * @returns Boolean
 */
export const regCommonChar = (str) => {
  return str?.match(/[\w`~!@#$%&\^\/ \u4e00-\u9fa5-]+/g)?.[0] || "";
};
