/* eslint-disable vue/component-definition-name-casing */
import "@/assets/icon/iconfont.css";
import TzIcon from "@/components/tzUI/TzIcon";
import TzText from "@/components/tzUI/TzText";
import TzImage from "@/components/tzUI/TzImage";
import TzView from "@/components/tzUI/TzView";
import TzButton from "@/components/tzUI/TzButton";
import TzInput from "@/components/tzUI/TzInput";
import TzForm from "@/components/tzUI/TzForm";
import TzSwitch from "@/components/tzUI/TzSwitch";
import TzCell from "@/components/tzUI/TzCell";
import TzPicker from "@/components/tzUI/TzPicker";
import TzRadioGroup from "@/components/tzUI/TzRadioGroup";
import TzScrollView from "@/components/tzUI/TzScrollView";
import TzTabs from "@/components/tzUI/TzTabs/index";
import TzTabPane from "@/components/tzUI/TzTabs/TabPane";
import TzSearch from "@/components/tzUI/TzSearch";
import TzAutocomplete from "@/components/tzUI/TzAutocomplete";
import TzPopup from "@/components/tzUI/TzPopup";
import TzSearchPicker from "@/components/tzUI/TzSearchPicker";
import TzSafeBottom from "@/components/tzUI/TzSafeBottom";

const tzComponents = {
  install(app) {
    app.component("tz-icon", TzIcon);
    app.component("tz-text", TzText);
    app.component("tz-image", TzImage);
    app.component("tz-view", TzView);
    app.component("tz-button", TzButton);
    app.component("tz-input", TzInput);
    app.component("tz-form", TzForm);
    app.component("tz-switch", TzSwitch);
    app.component("tz-cell", TzCell);
    app.component("tz-picker", TzPicker);
    app.component("tz-radio-group", TzRadioGroup);
    app.component("tz-scroll-view", TzScrollView);
    app.component("tz-tabs", TzTabs);
    app.component("tz-tab-pane", TzTabPane);
    app.component("tz-search", TzSearch);
    app.component("tz-autocomplete", TzAutocomplete);
    app.component("tz-popup", TzPopup);
    app.component("tz-search-picker", TzSearchPicker);
    app.component("tz-safe-bottom", TzSafeBottom);
  },
};
export default tzComponents;
