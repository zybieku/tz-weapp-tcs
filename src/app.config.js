export default {
  pages: [
    "pages/home/index",
    "pages/mine/index",
    "pages/login/index",
    "pages/login/login",
  ],
  subpackages: [
    {
      //sub 大陆子包
      root: "pages/subCN",
      pages: [
        "truck/home/index",
        "truck/addCar/index",
        "unbind/home/index",
        "confirm/home/index",
        "confirm/recordList/index",
        "confirm/details/index",
        "confirm/receipt/index",
        "confirm/singleShot/index",
        "confirm/changeType/index",
        "carQuery/home/index",
        "truck/trayInfo/index",
        "truck/boxingInfo/index",
      ],
    },
    {
      //sub 香港子包
      root: "pages/subHK",
      pages: [
        "ccrn/home/index",
        "ccrn/transport/index",
        "ccrn/licence/index",
        "ccrn/consigneer/index",
        "ccrnInquire/home/index",
        "ccrnInquire/receipt/index",
        "bundle/home/index",
        "bundle/addCar/index",
        "bundle/ccrn/index",
        "bundle/selectGoods/index",
        "bundleInquire/home/index",
        "bundleInquire/recordList/index",
        "bundleInquire/details/index",
        "bundleInquire/receipt/index",
        "unbind/home/index",
      ],
    },
    {
      //sub 用户中心自包
      root: "pages/subUser",
      pages: [
        "register/index",
        "findPwd/index",
        "setPwd/index",
        "concat/index",
        "webPage/index",
        "bill/index",
        "recharge/index",
      ],
    },
  ],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "陆运货物无缝清关系统",
    navigationBarTextStyle: "black",
    onReachBottomDistance: 50,
  },
  tabBar: {
    color: "#626567",
    selectedColor: "#1c78ef",
    backgroundColor: "#FBFBFB",
    borderStyle: "white",
    position: "bottom",
    list: [
      {
        pagePath: "pages/home/index",
        text: "首页",
        selectedIconPath: "assets/images/ic_confirm_active.png",
        iconPath: "assets/images/ic_confirm.png",
      },
      {
        pagePath: "pages/mine/index",
        text: "我的",
        selectedIconPath: "assets/images/ic_user_active.png",
        iconPath: "assets/images/ic_user.png",
      },
    ],
  },
};
