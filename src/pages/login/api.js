import api from "@/lib/tzAxios";

export const login = (data) => {
  const option = {
    url: "/auth/oauth/token",
    header: {
      Authorization: "Basic dHo6dHo=",
      systemId: "c07af764-a71c-46f3-9abd-0427a2555a4c",
      "content-type": "application/x-www-form-urlencoded",
    },
    data,
  };
  return api.post(option);
};

export const wxRegisterApi = (data) => {
  const option = {
    url: "/wx/wxRegister",
    data,
  };
  return api.post(option);
};

/**
 * 提供 wx.login code 给后端获取 openId
 * @param {*} data
 * @param tmpCode
 */
export const recordOpenIdApi = (data) => {
  const option = {
    url: "/wx/recordOpenId",
    data,
  };
  return api.post(option);
};
