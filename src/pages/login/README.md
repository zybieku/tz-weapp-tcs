# 登录页

#### 作者：林梓钿

#### 时间：2021-10-15

## 需求背景

登录页提供账号密码登录、注册页入口、找回密码页入口、版本号展示和客服电话  
4.8.3 增加微信快速登录


## 功能描述

- 账号密码输入表单
- 注册页、找回密码页入口按钮
- 客服电话：点击电话号码弹出拨打提示
- 微信快速登录  
  获取微信用户绑定的手机号码，通过手机号登录（不存在的账号则注册）  
  button 的 openType="getPhoneNumber"，在 onGetphonenumber 事件中可以获取到用户手机号的加密数据和密钥，把这两个数据 和 wx.login 获取到的code 传给后端获取手机号。接口成功返回后与账号密码登录操作一致

## 注意事项

- 回填账号名  
  从注册页、修改密码页、退出账号操作跳转到登录页时，需要带上已存在的账号回填。

- 本地缓存的 tz_user_info 增加 account property  
  接口 getBaseData 返回的 userInfo 中没有账号字段，如果是个人手机号登录，userInfo.name 字段存的是手机号，这时候 name 等于账号；  
	如果是其他类型（如管理员 gldadmin）账号，登录获取的 userInfo.name 是中文名称（如格蓝迪管理员）。  
	这种情况下缓存中没有账号，所以将登录时的账号一并添加进 storage。

- 用户类型缓存在 storage tz_dic_map 的 userType property
	- 1 isPersonal 个人司机
	- 2 isDriver 公司司机
	- 0 isOperator 公司操作员

- 微信小程序获取手机号码的监听事件名称是 onGetphonenumber 而不是 Taro 文档中的 buttonProps 的 onGetPhoneNumber

- 微信快速登录传给后端的小程序登录 code 需要提前获取，如果 code 刷新，手机号的加密数据也会更新，需要重新获取 code

## 待优化点

- 版本号动态更新  
  https://developers.weixin.qq.com/miniprogram/dev/api/open-api/account-info/wx.getAccountInfoSync.html