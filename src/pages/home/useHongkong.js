import Taro from "@tarojs/taro";
import { icHkWf, icCnQbcx, icCnZckb, icCnJb } from "@/assets";
export const useHongkong = (state) => {
  const hongkongList = [
    {
      imgUrl: icHkWf,
      url: "/pages/subHK/ccrn/home/index",
      title: "无缝(CCRN)",
      bgColor: "#5db75d",
      isNew: true,
    },
    {
      imgUrl: icCnZckb,
      url: "/pages/subHK/bundle/home/index",
      title: "香港捆绑",
      bgColor: "#fe8781",
    },
    {
      imgUrl: icCnJb,
      url: "/pages/subHK/unbind/home/index",
      title: "香港解绑",
      bgColor: "#ffc387",
    },
    {
      imgUrl: icCnQbcx,
      url: "/pages/subHK/ccrnInquire/home/index",
      title: "无缝(CCRN)查询",
      bgColor: "#77caf1",
    },
    {
      imgUrl: icCnQbcx,
      url: "/pages/subHK/bundleInquire/home/index",
      title: "香港捆绑查询",
      bgColor: "#77caf1",
    },
  ];
  const handleClick = (item) => {
    Taro.navigateTo({
      url: item.url,
    });
  };

  return () => (
    <tz-view class="section-hongkong">
      <tz-view class="section-title">香港段</tz-view>
      <tz-view class="grid">
        {hongkongList.map((item, index) => {
          return (
            <tz-view
              class="hongkong"
              key={index}
              onClick={() => handleClick(item)}
            >
              <tz-view class="item" style={{ backgroundColor: item.bgColor }}>
                <tz-image mode="widthFix" src={item.imgUrl}></tz-image>
                {item.isNew && <tz-view class="new-tag">NEW</tz-view>}
              </tz-view>
              <text class="title">{item.title}</text>
            </tz-view>
          );
        })}
      </tz-view>
    </tz-view>
  );
};
