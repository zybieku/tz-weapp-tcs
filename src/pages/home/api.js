import api from "@/lib/tzAxios";

export const getToken = (data) => {
  const option = {
    url: "/auth/oauth/token",
    header: {
      Authorization: "Basic dHo6dHo=",
      systemId: "c07af764-a71c-46f3-9abd-0427a2555a4c",
      "content-type": "application/x-www-form-urlencoded",
    },
    data,
  };
  return api.post(option);
};

export const coldQueryApi = (data) => {
  const option = {
    url: "/wx/carsApi/coldQuery",
    data,
  };
  return api.post(option);
};
