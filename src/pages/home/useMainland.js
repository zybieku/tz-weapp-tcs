import Taro from "@tarojs/taro";
import {
  icCnWgkckb,
  icCnYgkckb,
  icCnZckb,
  icCnQbcx,
  icCnClcx,
  icCnJb,
} from "@/assets";
export const useMainland = (state, navigateTo) => {
  const mainlandList = [
    {
      imgUrl: icCnWgkckb,
      url: "/pages/subCN/truck/home/index?type=1",
      title: "无柜空车捆绑",
      bgColor: "#5db75d",
    },
    {
      imgUrl: icCnYgkckb,
      url: "/pages/subCN/truck/home/index?type=2",
      title: "有柜空车捆绑",
      bgColor: "#5db75d",
    },
    {
      imgUrl: icCnZckb,
      url: "/pages/subCN/truck/home/index?type=3",
      title: "重车捆绑",
      bgColor: "#fe8781",
    },
    {
      imgUrl: icCnQbcx,
      url: "/pages/subCN/confirm/home/index",
      title: "确报查询",
      bgColor: "#77caf1",
    },
    {
      imgUrl: icCnClcx,
      url: "/pages/subCN/carQuery/home/index",
      title: "车辆查询",
      bgColor: "#77caf1",
    },
    {
      imgUrl: icCnJb,
      url: "/pages/subCN/unbind/home/index",
      title: "解绑",
      bgColor: "#ffc387",
    },
  ];
  const handleClick = (item) => {
    /**
     * 跳转到指定页面（不能跳转带tabbar底座的主页）
     *  path :页面路径
     *  query :需要传的对象，暂只支持一层对象
     */
    navigateTo({ path: item.url });
  };

  return () => (
    <tz-view class="section-mainland">
      <tz-view class="section-title">大陆段</tz-view>
      <tz-view class="grid">
        {mainlandList.map((item, index) => {
          return (
            <tz-view
              class="mainland"
              key={index}
              onClick={() => handleClick(item)}
            >
              <tz-view class="item" style={{ backgroundColor: item.bgColor }}>
                <tz-image mode="widthFix" src={item.imgUrl}></tz-image>
              </tz-view>
              <text class="title">{item.title}</text>
            </tz-view>
          );
        })}
      </tz-view>
    </tz-view>
  );
};
