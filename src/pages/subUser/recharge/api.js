import api from "@/lib/tzAxios";

export const wxPrePayApi = (data) => {
  const option = {
    url: "/wx/pay/prePay",
    data,
  };
  return api.post(option);
};

export const getUserUuidApi = (data) => {
  const option = {
    url: "/wx/pay/getUserUuid",
    data,
  };
  return api.post(option);
};
