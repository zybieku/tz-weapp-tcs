import Taro from "@tarojs/taro";
import { reactive } from "vue";
import { useTzRouter } from "@/vhooks/useTzRouter";
export const useUpdatePwd = (state, pageConfig) => {
  const switcherObj = reactive({
    switch1Checked: false,
    switch2Checked: false,
  });
  const validForm = () => {
    try {
      if (!state.accountForm.phone) {
        throw new Error("请输入手机号");
      }
      if (!state.accountForm.yzm) {
        throw new Error("请输入短信验证码");
      }
      if (!state.accountForm.pwd1) {
        throw new Error(pageConfig.noPwd1Text);
      }
      if (state.accountForm.pwd1.length < 4) {
        throw new Error(pageConfig.pwd1Less);
      }
      if (!state.accountForm.pwd2) {
        throw new Error(pageConfig.noPwd2Text);
      }
      if (state.accountForm.pwd2.length < 4) {
        throw new Error(pageConfig.pwd2Less);
      }
      if (state.accountForm.pwd1 !== state.accountForm.pwd2) {
        throw new Error("两次密码输入不一致");
      }
      return true;
    } catch (err) {
      Taro.showToast({
        title: err.message,
        icon: "none",
        duration: 2000,
      });
      return false;
    }
  };

  let submitBtnDisabled = false;

  const { reLaunch } = useTzRouter();
  const handleSubmit = () => {
    submitBtnDisabled = true;
    if (validForm()) {
      const { phone, yzm } = state.accountForm;
      const paramsObj = {
        phone,
        yzm,
        pwd: state.accountForm.pwd1,
      };
      pageConfig
        .submitApi(paramsObj)
        .then(() => {
          Taro.showToast({
            title: pageConfig.successText,
            icon: "none",
            duration: 2000,
          });
          setTimeout(() => {
            reLaunch({
              path: "/pages/login/index",
              query: { username: phone },
            });
          }, 1000);
        })
        .catch((err) => {
          submitBtnDisabled = false;
          Taro.showToast({
            title: err.data.msg,
            icon: "none",
            duration: 2000,
          });
        });
    }
  };
  return () => (
    <tz-view class="update-form">
      <tz-form>
        <tz-input
          v-model={state.accountForm.phone}
          placeholder="请输入手机号"
        />
      </tz-form>
      <tz-form>
        <tz-cell>
          <tz-input
            v-model={state.accountForm.yzm}
            placeholder="请输入短信验证码"
          />
        </tz-cell>
        <tz-cell>
          <tz-input
            type="text"
            password={!switcherObj.switch1Checked}
            v-model={state.accountForm.pwd1}
            placeholder={pageConfig.noPwd1Text}
          />
          <tz-switch v-model={switcherObj.switch1Checked}></tz-switch>
        </tz-cell>
        <tz-cell>
          <tz-input
            type="text"
            password={!switcherObj.switch2Checked}
            v-model={state.accountForm.pwd2}
            placeholder={pageConfig.noPwd2Text}
          />
          <tz-switch v-model={switcherObj.switch2Checked}></tz-switch>
        </tz-cell>
      </tz-form>
      <tz-view class="handle-block">
        {state.mode === "register" && (
          <tz-view class="protocol">
            注册即视为同意
            <tz-text
              onClick={() => {
                Taro.navigateTo({
                  url:
                    "/pages/subUser/webPage/index?url=" +
                    ENV_BASE_API +
                    "/register/list",
                });
              }}
            >
              《深圳泰洲一体化通关用户注册协议》
            </tz-text>
          </tz-view>
        )}
        <tz-button
          disabled={submitBtnDisabled}
          class="submit-btn"
          type="success"
          onClick={handleSubmit}
        >
          {pageConfig.submitBtnText}
        </tz-button>
      </tz-view>
    </tz-view>
  );
};
