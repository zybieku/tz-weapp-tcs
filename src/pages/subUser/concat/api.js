import api from "@/lib/tzAxios";

export const changePwd2Api = (data) => {
  const option = {
    url: "/wx/changePwd2",
    header: {
      systemId: "c07af764-a71c-46f3-9abd-0427a2555a4c",
      "content-type": "application/json",
    },
    data,
  };
  return api.post(option);
};
