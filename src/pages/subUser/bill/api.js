import api from "@/lib/tzAxios";

export const queryTradingApi = (data) => {
  const option = {
    url: "/wx/pay/queryTrading",
    header: {
      "content-type": "application/json",
    },
    data,
  };
  return api.post(option);
};
