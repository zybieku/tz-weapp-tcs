# 快速注册

#### 作者：林梓钿

#### 时间：2021-10-15

## 需求背景

通过手机号注册新用户，注册成功后用户角色为个人司机

## 功能描述

进入后会有两个界面

- 输入手机号获取验证码界面  
  此时只有输入手机号的 input 框和下一步，点击下一步获取验证码，切换到重置密码页面。切换到重置密码页时启动 60 秒计时，时间到了切回到获取验证码页面。

- 设置密码页面  
  通过短信验证码、新密码，提交设置密码，表单验证短信验证码是否正确，新密码需要管控长度不小于 4 和确认两次输入是否一致。

## 注意事项

- 这里的页面与找回密码页样式高度一致，所以将页面提出来放到 subUser/hooks 里面

```js
import { useSendMsgCode } from "../hooks/useSendMsgCode"; // 发送验证码页
import { useUpdatePwd } from "../hooks/useUpdatePwd"; // 修改密码页
```

## 待优化点

- 注册页有协议链接跳转

```js
var serverPath = "https://gatewaytest.sztaizhou.com";
// "https://xcx.sztaizhou.com";

<navigator
  url="../webpage/webpage?url={{serverPath}}/register/list"
  hover-class="navigator-hover"
>
  《深圳泰洲一体化通关用户注册协议》
</navigator>;
```
