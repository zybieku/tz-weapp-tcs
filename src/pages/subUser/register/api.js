import api from "@/lib/tzAxios";

export const sendValidCodeApi = (data) => {
  const option = {
    url: "/wx/sendValidCode",
    header: {
      systemId: "c07af764-a71c-46f3-9abd-0427a2555a4c",
      "content-type": "application/json",
    },
    data,
  };
  return api.post(option);
};

export const registerApi = (data) => {
  const option = {
    url: "/wx/register",
    header: {
      systemId: "c07af764-a71c-46f3-9abd-0427a2555a4c",
      "content-type": "application/json",
    },
    data,
  };
  return api.post(option);
};
