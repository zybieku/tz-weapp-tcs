export const useRecordListConfig = (passControlDic, billStatusDic) => {
  const handleData2Cell = (item) => {
    const cellList = [
      {
        title: "",
        value: item.docCode,
        mainCode: true,
      },
      {
        title: "捆绑编号(UBR)",
        value: item.ubr,
      },
      {
        title: "司机名称",
        value: item.driverName,
      },
      {
        title: "进/出口",
        value: { I: "大陆 -> 香港", E: "香港 -> 大陆" }[item.type],
      },
      {
        title: "车辆号码",
        value: item.vrn,
      },
      // {
      //   title: "过关管制站",
      //   value: item.vrn,
      // },
    ];

    passControlDic[item.custompt] &&
      cellList.push({
        title: "过关管制站",
        value: passControlDic[item.custompt],
      });

    cellList.push({
      title: "录单时间",
      value: item.createTime,
    });

    item.ccrns &&
      item.ccrns.length &&
      item.ccrns.forEach((cItem, cIndex) => {
        cellList.push({
          title: `CCRN${cIndex + 1}`,
          value: cItem,
          greenValue: true,
        });
      });

    return cellList;
  };

  const cellRightDataConfig = (itemData) => {
    const status = itemData.status;

    let keyName = "";
    if (status === "DRAFT") {
      // 删除
      keyName = "delete";
    } else if (!["DRAFT", "SENDING", "ISUNBUNDLING"].includes(status)) {
      // 查看回执
      keyName = "receipt";
    }

    const statusName = billStatusDic[status];

    let statusColor = "";
    if (status === "SENDING") {
      statusColor = "orange";
    } else if (["GOVTRECEIVED", "UNBUNDLINGSUCCESS"].includes(status)) {
      statusColor = "green";
    } else if (["UNBUNDLINGFAILURE", "ERROR", "UNKNOWN"].includes(status)) {
      statusColor = "red";
    } else {
      statusColor = "gray";
    }

    return {
      keyName,
      statusName,
      statusColor,
    };
  };

  return {
    handleData2Cell,
    cellRightDataConfig,
  };
};
