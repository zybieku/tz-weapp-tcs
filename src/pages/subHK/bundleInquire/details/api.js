import api from "@/lib/tzAxios";

// 进出口捆绑详情
export const getRocarsBundBeanByIdApi = (data) => {
  const option = {
    url: "/wx/rocarsBundApi/getRocarsBundBeanById",
    data,
  };
  return api.post(option);
};

// 确报解绑
export const unbindRocarsApi = (data) => {
  const option = {
    url: "/wx/rocarsBundApi/unbindRocars",
    data,
  };
  return api.post(option);
};

// 提交香港捆绑
export const callRocarsBundApi = (data) => {
  const option = {
    url: "/wx/rocarsBundApi/callRocarsBund",
    data,
  };
  return api.post(option);
};

//订阅消息模板id
export const templateIdList = ["Yqed7miyggZBTRHjB4Cl52t-JFl18H3KborNoVxlNMg"];
