import api from "@/lib/tzAxios";

// 查看进出口捆绑回执信息
// export const receiptApi = (data) => {
//   const option = {
//     url: "/wx/rocarsBundApi/receipt",
//     data,
//   };
//   return api.post(option);
// };

// 查看回执 NOTE: 后端让使用香港无缝的查看回执接口，说是一样的
export const receiptApi = (id) => {
  const option = {
    url: `/wx/wxHkBaseDataApi/listSysStatus/${id}/HKUBR`,
  };
  return api.post(option);
};
