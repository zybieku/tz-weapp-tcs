import Taro from "@tarojs/taro";
export const useQueryForm = (state, getDicMap, navigateTo) => {
  const { billStatusList, ieTypeList } = getDicMap();

  const queryForm = state.queryForm;

  const searchClick = () => {
    console.log(queryForm);
    navigateTo({
      path: "/pages/subHK/bundleInquire/recordList/index",
      query: queryForm,
    });
  };

  const handleScan = () => {
    Taro.scanCode()
      .then((res) => {
        state.queryForm.docCode = res.result;
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return () => (
    <tz-view class="query-block">
      <tz-form class="query-form">
        <tz-cell title="进/出口" border>
          <tz-picker
            v-model={queryForm.type}
            mode="selector"
            range={ieTypeList}
            rangeKey="para_name"
            valueKey="para_value"
          ></tz-picker>
        </tz-cell>
        <tz-cell title="单据状态" border>
          <tz-picker
            v-model={queryForm.status}
            mode="selector"
            range={billStatusList}
            rangeKey="para_name"
            valueKey="para_value"
          ></tz-picker>
        </tz-cell>
        <tz-cell title="录单员" border>
          <tz-input
            v-model={queryForm.createLoginName}
            type="text"
            placeholder="录单员"
            cleable
          />
        </tz-cell>
        <tz-cell title="六联单号" border>
          <tz-input
            v-model={queryForm.docCode}
            type="number"
            placeholder="六联单号"
            cleable
            maxlength="13"
          />
          <tz-icon name="scan" onClick={() => handleScan()}></tz-icon>
        </tz-cell>
        <tz-cell title="录单起始时间" border>
          <tz-picker v-model={queryForm.beginTime} mode="date">
            <text>
              {queryForm.beginTime ? (
                <text>{queryForm.beginTime}</text>
              ) : (
                <text class="placeholder">点击选择开始日期</text>
              )}
            </text>
          </tz-picker>
        </tz-cell>
        <tz-cell title="录单结束时间" border>
          <tz-picker v-model={queryForm.endTime} mode="date">
            {queryForm.endTime ? (
              <text>{queryForm.endTime}</text>
            ) : (
              <text class="placeholder">点击选择结束日期</text>
            )}
          </tz-picker>
        </tz-cell>
      </tz-form>
      <tz-button
        class="search-btn"
        type="primary"
        onClick={() => searchClick()}
      >
        搜索
      </tz-button>
    </tz-view>
  );
};
