import api from "@/lib/tzAxios";

// 进出口捆绑查询
export const queryRocarsBundPageApi = (data) => {
  const option = {
    url: "/wx/rocarsBundApi/queryRocarsBundPage",
    data,
  };
  return api.post(option);
};

// 删除进出口捆绑
export const deleteRocarsBundApi = (data) => {
  const option = {
    url: "/wx/rocarsBundApi/deleteRocarsBund",
    data,
  };
  return api.post(option);
};
