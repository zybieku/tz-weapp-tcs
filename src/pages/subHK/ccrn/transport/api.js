import api from "@/lib/tzAxios";

/**
 * 保存/修改香港货物
 * param {obj} data
 */
export const saveOrUpdateUrl = (data) => {
  const option = {
    url: "/wx/wxHkRocarsApi/saveOrUpdate",
    data: data,
  };
  return api.post(option);
};
