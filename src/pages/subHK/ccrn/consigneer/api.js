import api from "@/lib/tzAxios";

/**
 * 搜索查询收发货人信息
 * param {obj} data
 */
export const consigneeConsigner = (name) => {
  const option = {
    url: "/wx/wxHkBaseDataApi/likeSearchConsigneeConsigner/" + name,
  };
  return api.post(option);
};

/**
 * 搜索查询产品编号信息
 * param {obj} data
 */
export const likeSearchBaseCargos = (name) => {
  const option = {
    url: "/wx/wxHkBaseDataApi/likeSearchBaseCargos/" + name,
  };
  return api.post(option);
};
