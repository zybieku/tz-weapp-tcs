import api from "@/lib/tzAxios";

/**
 * 保存/修改香港货物
 * param {obj} data
 */
export const saveOrUpdateApi = (data) => {
  const option = {
    url: "/wx/wxHkRocarsApi/saveOrUpdate",
    data,
  };
  return api.post(option);
};

/**
 * 申报
 * param {obj} data
 * route
 * id
 */
export const applyApi = (data) => {
  const option = {
    url: `/wx/wxHkRocarsApi/apply/${data}`,
    // data,
  };
  return api.post(option);
};

/**
 * 保存收发货人
 * param {obj} data
 * name         收发货人名称
 * address      收发货人地址
 * countryCode  收发货人国家代码
 * countryName  收发货人国家名称
 */
export const saveConsigneeConsignerApi = (data) => {
  const option = {
    url: "/wx/wxHkBaseDataApi/saveConsigneeConsigner",
    data,
  };
  return api.post(option);
};

/**
 * 保存基础货物信息
 * param {obj} data
 * cargoCode  产品编号
 * cargoDesc  货物说明
 * packType   种类
 * packUnit   包装单位
 */
export const saveBaseCargosApi = (data) => {
  const option = {
    url: "/wx/wxHkBaseDataApi/saveBaseCargos",
    data,
  };
  return api.post(option);
};

/**
 * 老校验接口: 保存时的校验
 * @param {obj} data 数据与保存接口一致
 */
export const saveValidateApi = (data) => {
  const option = {
    url: "/wx/wxHkRocarsApi/saveValidate",
    data,
  };
  return api.post(option);
};

/**
 * 查看详情
 * param {string} id
 */
export const detailApi = (id) => {
  const option = {
    url: `/wx/wxHkRocarsApi/detail/${id}`,
  };
  return api.post(option);
};

/**
 * 获取余额和费用
 * url 后面带的 {costCode} 200501 是香港ccrn的
 */
export const getFeeAgreeVOByCustRegNoApi = () => {
  const option = {
    url: "/wx/wxHkBaseDataApi/getFeeAgreeVOByCustRegNo/200501",
  };
  return api.post(option);
};

//订阅消息模板id
export const templateIdList = ["Yqed7miyggZBTRHjB4Cl52t-JFl18H3KborNoVxlNMg"];
