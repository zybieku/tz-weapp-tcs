export const useTabHeader = (state, queryBundRocarsData, letgetDay) => {
  const tabList = [
    {
      label: "七天内",
      name: "7",
    },
    {
      label: "一个月内",
      name: "30",
    },
    {
      label: "条件查询",
      name: "other",
    },
  ];

  const onTabClick = (tabInfo) => {
    state.activedTabName = tabInfo.name;
    if (tabInfo.name !== "other") {
      state.currentPage = 1;
      const params = {
        beginTime: letgetDay(-tabInfo.name),
        currentPage: 1,
        endTime: letgetDay(0),
      };
      queryBundRocarsData(params);
    }
  };

  return () => (
    <tz-view class="tabs__header">
      {tabList.map((item) => (
        <tz-view
          class={[
            "tab__pane",
            state.activedTabName === item.name ? "actived" : "",
          ]}
          onClick={() => onTabClick(item)}
        >
          <text>{item.label}</text>
        </tz-view>
      ))}
    </tz-view>
  );
};
