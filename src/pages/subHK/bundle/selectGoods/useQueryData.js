import { getDicMap } from "@/utils/storage";
export const useQueryData = (state) => {
  const dicMap = getDicMap();
  return () => (
    <tz-view class="data-list">
      {state.dataList.map((item, index) => (
        <tz-view class="data-list-item" key={index}>
          <tz-view class="info-area">
            <tz-view class="main-code">{item.docCode}</tz-view>
            <tz-cell title={index + 1 + "-CCRN->"}>{item.ccrn}</tz-cell>
            <tz-cell title="进/出口">{dicMap.ieTypeDic[item.type]}</tz-cell>

            <tz-cell title="发货人">{item.cnsgrName1}</tz-cell>
            <tz-cell title="收货人">{item.cnsgeName1}</tz-cell>
            <tz-cell title="预计到达时间">{item.deparrdt}</tz-cell>
          </tz-view>
          <tz-view class="operation-area">
            <tz-view>是否选取</tz-view>
            <tz-view class="handle-btn-content">
              <tz-switch type="switch" v-model={item.isCheck}></tz-switch>
            </tz-view>
          </tz-view>
        </tz-view>
      ))}
    </tz-view>
  );
};
