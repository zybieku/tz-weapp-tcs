import Taro from "@tarojs/taro";
export const useQueryForm = (state, queryBundRocarsData) => {
  const searchClick = () => {
    state.currentPage = 1;
    const params = {
      ccrn: state.ccrn,
      currentPage: 1,
    };
    queryBundRocarsData(params);
    state.activedTabName = "7";
  };
  const handleScan = () => {
    Taro.scanCode()
      .then((res) => {
        state.queryForm.docCode = res.result;
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return () => (
    <tz-view class="query-block">
      <tz-form class="query-form">
        <tz-cell title="CCRN" border>
          <tz-input
            v-model={state.ccrn}
            type="text"
            placeholder="CCRN"
            cleable
          />
          <tz-icon name="scan" onClick={() => handleScan()}></tz-icon>
        </tz-cell>
      </tz-form>
      <tz-button
        class="search-btn"
        type="success"
        onClick={() => searchClick()}
      >
        搜索
      </tz-button>
    </tz-view>
  );
};
