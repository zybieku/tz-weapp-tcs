import api from "@/lib/tzAxios";

/**
 * 查询进出口货物
 * param {obj} data
 */
export const getNoBundRocars = (data) => {
  const option = {
    url: "/wx/rocarsBundApi/getNoBundRocars",
    data: data,
  };
  return api.post(option);
};
