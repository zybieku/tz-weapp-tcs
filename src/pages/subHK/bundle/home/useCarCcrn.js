import Taro from "@tarojs/taro";
import { useTzRouter } from "@/vhooks/useTzRouter";
export const useCarCcrn = (state, dicMap) => {
  const { navigateTo } = useTzRouter();
  //选择车辆
  const cardClick = () => {
    navigateTo({
      path: "/pages/subHK/bundle/addCar/index",
    });
  };
  //录入ccrn
  const addCcrn = () => {
    navigateTo({
      path: "/pages/subHK/bundle/ccrn/index",
    });
  };
  //编辑车辆
  const editCar = () => {
    navigateTo({
      path: "/pages/subHK/bundle/addCar/index",
      query: { carInfo: JSON.stringify(state.carInfo) },
    });
  };

  //删除ccrn
  const delCcrn = (index) => {
    Taro.showModal({
      title: "",
      content: "确定要删除该记录吗？",
      success: function (res) {
        if (res.confirm) {
          state.ccrnList.splice(index, 1);
        }
      },
    });
  };
  //编辑ccrn
  const editCcrn = () => {
    navigateTo({
      path: "/pages/subHK/bundle/ccrn/index",
      query: { ccrnInfo: JSON.stringify(state.ccrnList) },
    });
  };

  return () => (
    <>
      <tz-view class="section ptb5">
        <tz-cell title="车辆信息" border class="text-right">
          <tz-button type="primary" size="small" onClick={cardClick}>
            车辆选择
          </tz-button>
        </tz-cell>
        <tz-view class="car-info" onClick={editCar} v-show={state.isShowCar}>
          <tz-view class="car-data">
            <tz-view>
              <tz-text>香港车牌</tz-text>
              {state.carInfo.hkTruckLicenseName}
            </tz-view>
            <tz-view>
              <tz-text>司机姓名</tz-text>
              {state.carInfo.driverName}
            </tz-view>
            <tz-view>
              <tz-text>rocars帐号</tz-text>
              {state.carInfo.hkDriverCode}
            </tz-view>
            <tz-view>
              <tz-text>大陆手机号</tz-text>
              {state.carInfo.dMobile}
            </tz-view>
            <tz-view>
              <tz-text>香港手机号</tz-text>
              {state.carInfo.hkDMobile}
            </tz-view>
          </tz-view>
          <tz-icon name="arrow-right"></tz-icon>
        </tz-view>
      </tz-view>
      <tz-view class="section">
        <tz-cell title="进/出口类型" border required>
          <tz-picker
            mode="selector"
            v-model={state.type}
            range={dicMap.ieTypeList}
            rangeKey="para_name"
            valueKey="para_value"
          ></tz-picker>
        </tz-cell>
      </tz-view>
      <tz-view class="section ptb5" v-show={state.isShowCar}>
        <tz-cell title="货物信息" border required class="text-right">
          <tz-button
            v-show={state.ccrnList.length === 0}
            type="primary"
            size="small"
            onClick={addCcrn}
          >
            录入CCRN码
          </tz-button>
        </tz-cell>
        {state.ccrnList.map((item, index) => (
          <tz-view class="ccrn-info" key={index}>
            <tz-view class="ccrn-data" onClick={editCcrn}>
              <text>{index + 1 + "-CCRN->"}</text>
              {item}
            </tz-view>
            <tz-view class="ccrn-del" onClick={() => delCcrn(index)}>
              <tz-icon name="close-plain"></tz-icon>
            </tz-view>
          </tz-view>
        ))}
      </tz-view>
    </>
  );
};
