import Taro from "@tarojs/taro";
import { reactive } from "vue";
import { getDicMap } from "@/utils/storage";
import { useTzRouter } from "@/vhooks/useTzRouter";
export const useOtherInfo = (state) => {
  const dicMap = getDicMap();
  const otherInfo = reactive({
    iconName: "arrow-bottom",
    isShow: false,
  });
  const handleOpen = () => {
    otherInfo.iconName =
      otherInfo.iconName === "arrow-top" ? "arrow-bottom" : "arrow-top";
    otherInfo.isShow = otherInfo.iconName === "arrow-top" ? true : false;
  };
  const handleScan = () => {
    Taro.scanCode({
      success: function (res) {
        state.docCode = res.result;
      },
      fail: function () {},
    });
  };
  const changeParaName = (e) => {
    state.deparrdt = e.detail.value;
  };
  //删除货物信息
  const delGoods = (index) => {
    Taro.showModal({
      title: "",
      content: "确定要删除该记录吗？",
      success: function (res) {
        if (res.confirm) {
          state.goodsList.splice(index, 1);
        }
      },
    });
  };
  const { navigateTo } = useTzRouter();
  //选择货物信息
  const selectGoods = () => {
    const goodsArr = [];
    state.goodsList.forEach((item) => {
      goodsArr.push(item.id);
    });
    state.goodsIds = goodsArr; //货物信息ids
    navigateTo({
      path: "/pages/subHK/bundle/selectGoods/index",
      query: { goodsArr: JSON.stringify(goodsArr) },
    });
  };

  return () => (
    <tz-view class="section other-info">
      <tz-cell title="其他" border class="text-right" onClick={handleOpen}>
        <tz-icon name={otherInfo.iconName}></tz-icon>
      </tz-cell>
      <tz-cell title="六联单号" border class="fix" v-show={otherInfo.isShow}>
        <tz-input
          v-model={state.docCode}
          type="number"
          placeholder="请输入六联单号"
          clearable
          maxlength="13"
        />
        <tz-icon name="scan" onClick={handleScan}></tz-icon>
      </tz-cell>
      <tz-cell title="过关管制站" border v-show={otherInfo.isShow}>
        <tz-picker
          mode="selector"
          v-model={state.custompt}
          range={dicMap.passControlList}
          rangeKey="para_name"
          valueKey="para_value"
        ></tz-picker>
      </tz-cell>
      <tz-cell title="货运日期" v-show={otherInfo.isShow}>
        <tz-picker mode="date" onChange={changeParaName}>
          {state.deparrdt || "点击选择货运日期"}
        </tz-picker>
      </tz-cell>
      {state.goodsList.map((item, index) => (
        <tz-view class="ccrn-info" key={index}>
          <tz-view class="ccrn-data orther-data" onClick={selectGoods}>
            <tz-view>
              <text>{state.ccrnList.length + index + 1 + "-CCRN->"}</text>
              <text class="blue">{item.ccrn}</text>
            </tz-view>
            <tz-view>
              <text>进/出口</text>
              {dicMap.ieTypeDic[item.type]}
            </tz-view>
            <tz-view>
              <text>发货人</text>
              {item.cnsgrName1}
            </tz-view>
            <tz-view>
              <text>收货人</text>
              {item.cnsgeName1}
            </tz-view>
            <tz-view>
              <text>预计到达时间</text>
              {item.deparrdt}
            </tz-view>
          </tz-view>
          <tz-view class="ccrn-del" onClick={() => delGoods(index)}>
            <tz-icon name="close-plain"></tz-icon>
            删除
          </tz-view>
        </tz-view>
      ))}
    </tz-view>
  );
};
