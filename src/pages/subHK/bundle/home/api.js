import api from "@/lib/tzAxios";

/**
 * 保存捆绑信息
 * param {obj} data
 */
export const saveOrSubmitUrl = (data) => {
  const option = {
    url: "/wx/rocarsBundApi/saveOrSubmitBundRocars",
    data: data,
  };
  return api.post(option);
};
