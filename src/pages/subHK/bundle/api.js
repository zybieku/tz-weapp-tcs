import api from "@/lib/tzAxios";

/**
 * 根据id捆绑信息
 * param {string} id
 */
export const getRocarsBundBeanById = (id) => {
  const option = {
    url: "/wx/rocarsBundApi/getRocarsBundBeanById",
    data: id,
  };
  return api.post(option);
};
