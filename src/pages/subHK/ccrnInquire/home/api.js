import api from "@/lib/tzAxios";

/**
 * 保存捆绑信息
 * param {obj} data
 */
export const saveOrSubmitUrl = (data) => {
  const option = {
    url: "/wx/rocarsBundApi/saveOrSubmitBundRocars",
    data: data,
  };
  return api.post(option);
};

/**
 * 默认查询列表
 */
export const getList = (data) => {
  const option = {
    url: "/wx/wxHkRocarsApi/list",
    data: data,
  };
  return api.post(option);
};

/**
 * 复制香港无缝
 */
export const copyRocarsById = (id) => {
  const option = {
    url: "/wx/wxHkRocarsApi/copyRocarsById/" + id,
  };
  return api.post(option);
};

/**
 * 删除香港无缝
 */
export const deleteRocarsById = (id) => {
  const option = {
    url: "/wx/wxHkRocarsApi/deleteRocarsById/" + id,
  };
  return api.post(option);
};

/**
 * 分页查询ccrn
 */
export const postWxHkCcrnList = (data) => {
  const option = {
    url: "/wx//wxHkRocarsApi/listCcrn",
    data,
  };
  return api.post(option);
};
