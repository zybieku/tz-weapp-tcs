import api from "@/lib/tzAxios";

// 查看香港无缝(CCRN)回执信息
export const receiptApi = (id) => {
  const option = {
    url: `/wx/wxHkBaseDataApi/listSysStatus/${id}/HKCCRN`,
  };
  return api.post(option);
};
