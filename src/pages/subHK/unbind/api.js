import api from "@/lib/tzAxios";

/**
 * 默认查询列表
 */
export const getList = (data) => {
  const option = {
    url: "/wx/rocarsBundApi/queryRocarsBundPage",
    data: data,
  };
  return api.post(option);
};
//订阅
export const saveSubMessage = (data) => {
  const option = {
    url: "/wx/user/saveSubscribeMessage",
    data: data,
  };
  return api.post(option);
};
//解绑
export const unbind = (data) => {
  const option = {
    url: "/wx/rocarsBundApi/unbindRocars",
    header: {
      systemId: "c07af764-a71c-46f3-9abd-0427a2555a4c",
    },
    data: data,
  };
  return api.post(option);
};
