import api from "@/lib/tzAxios";

export const logoutApi = (data) => {
  const option = {
    url: "/uc/account/logout",
    header: {
      systemId: "c07af764-a71c-46f3-9abd-0427a2555a4c",
    },
    data,
  };
  return api.post(option);
};
