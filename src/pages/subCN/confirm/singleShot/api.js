import api from "@/lib/tzAxios";

//查看回执
export const lookReceiptApi = (data) => {
  const option = {
    url: "/wx/receiptApi/lookReceipt",
    data,
  };
  return api.post(option);
};
