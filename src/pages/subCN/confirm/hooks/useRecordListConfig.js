export const useRecordListConfig = (inOutTypeDic, customsCode, statusDic) => {
  const handleData2Cell = (item) => {
    return [
      {
        title: "",
        value: item.docCode,
        mainCode: true,
      },
      {
        title: "司机名称",
        value: item.driverName,
      },
      {
        title: "单据类型",
        value: inOutTypeDic[item.inOutType],
      },
      {
        title: "口岸代码",
        value: customsCode[item.customsCode]
          ? customsCode[item.customsCode]
          : item.customsCode,
      },
      {
        title: "录单时间",
        value: item.createTime,
      },
    ];
  };

  const cellRightDataConfig = (itemData) => {
    const status = itemData.consistStatus;

    let keyName = "";
    if (status === "00") {
      // 删除
      keyName = "delete";
    } else if (["00", "30", "50", "31"].indexOf(status) === -1) {
      // 查看回执
      keyName = "receipt";
    } else if (status === "31") {
      // 回执/截图
      keyName = "receiptAndShot";
    }

    const statusName = statusDic[itemData.consistStatus];

    let statusColor = "";
    if (itemData.consistStatus === "30") {
      statusColor = "orange";
    } else if (["31", "51"].includes(itemData.consistStatus)) {
      statusColor = "green";
    } else if (["32", "52"].includes(itemData.consistStatus)) {
      statusColor = "red";
    } else {
      statusColor = "gray";
    }

    return {
      keyName,
      statusName,
      statusColor,
    };
  };

  return {
    handleData2Cell,
    cellRightDataConfig,
  };
};
