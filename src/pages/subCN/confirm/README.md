# 确报查询

#### 作者：林梓钿

#### 时间：2021-10-20

## 需求背景

提供查询空车捆绑、搭载空箱捆绑、重车捆绑保存后的数据

## 项目文件和结构

confirm  
├─home 主页
├─singleShot 查看截图
├─details 详情页
├─changeType 变更单据类型
├─receipt 回执/截图  
└─recordList 确报查询

## 功能描述

#### 1.home 主页

1. 主页顶部提示区域，点击其中的海关确报查询文案跳转至对应页面
2. tab 区域，提供七天内、一个月内、条件查询三个 tab，七天内和一个月内查询对应时间区间的数据列表，条件查询页面提供表单录入查询条件搜索确报数据。

- 列表页通过组件 RecordList 展示
  - dataList 列表数据
  - cellList 列表项左侧展示数据处理
  - onBtnClick 操作按钮回调
  - listItemClick 列表项点击事件

```vue
// pages/subCN/confirm/home/index.vue

<record-list
  dataList="{state.confirmDataList}"
  cellList="{handleData2Cell}"
  onBtnClick="{handleBtnClick}"
  onItemClick="{listItemClick}"
  cellRightDataConfig="{cellRightDataConfig}"
></record-list>
```

#### 2.recordList 确报查询列表页

主页通过条件查询进入此页面，展示查询结果  
列表页与 home 主页一样调用了 RecordList 组件

#### 3.details 详情页

展示大陆确报的详情数据，根据不同状态提供对应的操作按钮

#### 4.receipt 回执/截图

从确报查询列表页点击查看回执或回执截图按钮进入，页面展示回执详情列表。当捆绑状态为 31 捆绑成功时，展示单一截图操作按钮。

下载pdf按钮点击后通过接口返回的路由下载 pdf，之后打开 pdf 文件。

#### 5.singleShot 查看截图

从回执页面点击查看截图进入，路由参数带截图数据，截图页面展示从路由获取的截图。下载pdf按钮的操作与回执页面的相同。

#### 6.changeType 变更单据类型

在确报详情页的草稿状态时，单据类型可以变更，点击变更进入此页面。默认选中当前的类型，变更其他类型后点击确定，跳转到对应的编辑页面。

## 注意事项

- 确报查询主页中的列表页和条件查询出来的列表页同样都是使用了 RecordList 组件，除了事件回调，传入 RecordList 组件的 props 相同，所以把他们放到 confirm 目录下的 hooks/useRecordListConfig.js 文件中。

## 待优化点

- 存在数据量大同时在频繁加载分页数据的时候页面空白的情况
