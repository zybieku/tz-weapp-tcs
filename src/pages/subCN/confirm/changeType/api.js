import api from "@/lib/tzAxios";

//查看回执
export const lookReceiptApi = (data) => {
  const option = {
    url: "/wx/receiptApi/lookReceipt",
    data,
  };
  return api.post(option);
};

// 确报单一截图
export const singleShotApi = (data) => {
  const option = {
    url: "/wx/consist/singleShot",
    data,
  };
  return api.post(option);
};
