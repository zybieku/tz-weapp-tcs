import api from "@/lib/tzAxios";

export const getConsistBeanByIdApi = (data) => {
  const option = {
    url: "/wx/consistQueryApi/getConsistBeanById",
    data,
  };
  return api.post(option);
};

// 确报解绑
export const deleteCellConsistApi = (data) => {
  const option = {
    url: "/wx/consist/deleteCellConsist",
    data,
  };
  return api.post(option);
};

// 确报逻辑校验url
export const checkItemBundleApi = (data) => {
  const option = {
    url: "/wx/consist/checkItemBundle",
    data,
  };
  return api.post(option);
};

// 确报捆绑
export const callConsistApi = (data) => {
  const option = {
    url: "/wx/consist/callConsist",
    data,
  };
  return api.post(option);
};

//订阅消息模板id
export const templateIdList = ["Yqed7miyggZBTRHjB4Cl52t-JFl18H3KborNoVxlNMg"];
