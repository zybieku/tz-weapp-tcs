export const useTruckBox = (state, getDicMap) => {
  const { boxingSizeCode, fullOrEmptyCode } = getDicMap();

  return () => (
    <tz-view class="info-box">
      <tz-view class="info-box-title">集装箱信息</tz-view>
      <tz-view class="cell-content">
        {state.consistBean.truckList[0].truckBoxings.map((item, index) => (
          <>
            <tz-cell border title={`${index + 1}-集装箱号->`}>
              {item.boxingCode}
            </tz-cell>
            <tz-cell border title="集装箱类型尺寸">
              {item.boxingSizeType}
            </tz-cell>
            <tz-cell border title="集装箱来源">
              {boxingSizeCode[item.boxingSourceCode]}
            </tz-cell>
            {state.inOutTypeDiffType === 3 && (
              <tz-cell border title="重箱/空箱">
                {fullOrEmptyCode[item.fullOrEmptyCode]}
              </tz-cell>
            )}
            <tz-cell border title="集装箱自重">
              {item.boxingWeight}
            </tz-cell>
            {state.inOutTypeDiffType === 3 && item.boxingSealed && (
              <tz-cell border title="封志信息">
                {item.boxingSealed}
              </tz-cell>
            )}
          </>
        ))}
      </tz-view>
    </tz-view>
  );
};
