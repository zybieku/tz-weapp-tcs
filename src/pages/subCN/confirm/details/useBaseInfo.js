// 固定信息
export const useBaseInfo = (state, userType, getDicMap, navigateTo) => {
  const {
    inOutTypeDic,
    customsCode: customsCodeDic,
    countryCode,
    goodsCustomsCode: goodsCustomsCodeDic,
    statusDic,
  } = getDicMap();

  // 变更单据类型
  const changeType = () => {
    navigateTo({
      path: "/pages/subCN/confirm/changeType/index",
      query: { consistId: state.consistId },
    });
  };

  const { consistBean } = state;

  const truckNameList = ["吨车", "货柜车"];
  const healthStatusList = ["异常", "正常"];
  const booleanCN_List = ["否", "是"];

  return () => (
    <tz-view class="info-box">
      <tz-view class="info-box-title">{`当前状态：${
        statusDic[consistBean.status]
      }`}</tz-view>
      <tz-view class="cell-content">
        <tz-cell border title="内地车牌">
          {consistBean.truckList[0].truckLicenseName}
        </tz-cell>
        <tz-cell border title="车辆海关编码">
          {consistBean.truckList[0].truckLicenseCode}
        </tz-cell>
        <tz-cell border title="车辆类型">
          {truckNameList[consistBean.truckList[0].truckType] ||
            consistBean.truckList[0].truckType}
        </tz-cell>
        <tz-cell border title="司机姓名">
          {consistBean.truckList[0].driverName}
        </tz-cell>
        <tz-cell border title="司机卡号">
          {consistBean.truckList[0].driverCode}
        </tz-cell>
        <tz-cell border title="司机联系方式">
          {consistBean.truckList[0].dMobile}
        </tz-cell>
        {!userType.isPersonal && (
          <tz-cell border title="车辆牌头公司">
            {consistBean.truckList[0].belongName}
          </tz-cell>
        )}
        <tz-cell border title="单据类型">
          {inOutTypeDic[consistBean.inOutType]}
          {["00", "03", "32", "51"].includes(consistBean.status) && (
            <tz-view
              class="sub-content"
              onClick={() => {
                changeType();
              }}
            >
              变更
            </tz-view>
          )}
        </tz-cell>
        <tz-cell border title="六联单号">
          {consistBean.docCode}
        </tz-cell>
        <tz-cell border title="口岸代码">
          {customsCodeDic[consistBean.customsCode] || consistBean.customsCode}
        </tz-cell>
        <tz-cell border title="到达日期时间">
          {consistBean.deparrdt}
        </tz-cell>
        {["MT4401", "MT4403", "MT4405"].includes(consistBean.inOutType) && (
          <>
            <tz-cell border title="司乘人员数">
              {consistBean.truckList[0].numStaffMember}
            </tz-cell>
            <tz-cell border title="健康状态">
              {healthStatusList[consistBean.truckList[0].healthStatus] ||
                consistBean.truckList[0].healthStatus}
            </tz-cell>
            <tz-cell border title="有预防接种">
              {booleanCN_List[consistBean.truckList[0].vaccinate] ||
                consistBean.truckList[0].vaccinate}
            </tz-cell>
            <tz-cell border title="途径国家(地区)">
              {countryCode[consistBean.truckList[0].rountingCountryCode]}
            </tz-cell>
          </>
        )}
        {state.inOutTypeDiffType === 2 && (
          <>
            <tz-cell border title="装/卸货地代码">
              {consistBean.unloadingAddressCode}
            </tz-cell>
            <tz-cell border title="货物通关代码">
              {goodsCustomsCodeDic[consistBean.goodsCustomsCode]}
            </tz-cell>
          </>
        )}
        {state.inOutTypeDiffType === 3 && !userType.isPersonal && (
          <tz-cell border title="其它">
            {consistBean.others}
          </tz-cell>
        )}
      </tz-view>
    </tz-view>
  );
};
