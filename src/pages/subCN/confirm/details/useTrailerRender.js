export const useTrailerRender = (state, getDicMap, getColdCompany) => {
  const { trailerTypeCode } = getDicMap();
  const { consistBean } = state;
  const { isColdCompany } = getColdCompany();

  return () => (
    <tz-view class="info-box">
      <tz-view class="info-box-title">托架信息</tz-view>
      <tz-view class="cell-content">
        <tz-cell border title="托架号">
          {consistBean.truckList[0].trailerCode}
        </tz-cell>
        <tz-cell border title="托架规格">
          {trailerTypeCode[consistBean.truckList[0].trailerTypeCode]}
        </tz-cell>
        <tz-cell border title="架重(KG)">
          {consistBean.truckList[0].trailerWeight}
        </tz-cell>
        {isColdCompany === "01" && (
          <tz-cell border title="发电机重量(KG)">
            {consistBean.truckList[0].generatorWeight}
          </tz-cell>
        )}
      </tz-view>
    </tz-view>
  );
};
