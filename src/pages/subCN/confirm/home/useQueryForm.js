import Taro from "@tarojs/taro";
export const useQueryForm = (state, getDicMap, navigateTo) => {
  const { customsCodeList, inOutTypeDicList, statusDicList } = getDicMap();

  const queryForm = state.queryForm;

  const searchClick = () => {
    navigateTo({
      path: "/pages/subCN/confirm/recordList/index",
      query: queryForm,
    });
  };

  const handleScan = () => {
    Taro.scanCode()
      .then((res) => {
        state.queryForm.docCode = res.result;
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return () => (
    <tz-view class="query-block">
      <tz-form class="query-form">
        <tz-cell title="单据类型" border>
          <tz-picker
            v-model={queryForm.inOutType}
            mode="selector"
            range={inOutTypeDicList}
            rangeKey="para_name"
            valueKey="para_value"
          ></tz-picker>
        </tz-cell>
        <tz-cell title="口岸代码" border>
          <tz-picker
            v-model={queryForm.customsCode}
            mode="selector"
            range={customsCodeList}
            rangeKey="para_name"
            valueKey="para_value"
          ></tz-picker>
        </tz-cell>
        <tz-cell title="单据状态" border>
          <tz-picker
            v-model={queryForm.status}
            mode="selector"
            range={statusDicList}
            rangeKey="para_name"
            valueKey="para_value"
          ></tz-picker>
        </tz-cell>
        <tz-cell title="司机姓名" border>
          <tz-input
            v-model={queryForm.driverName}
            type="text"
            placeholder="司机姓名"
            cleable
          />
        </tz-cell>
        <tz-cell title="六联单号" border>
          <tz-input
            v-model={queryForm.docCode}
            type="number"
            placeholder="六联单号"
            maxlength="13"
            cleable
          />
          <tz-icon name="scan" onClick={() => handleScan()}></tz-icon>
        </tz-cell>
        <tz-cell title="录单时间" border>
          <tz-picker v-model={queryForm.beginTime} mode="date">
            <text>
              {queryForm.beginTime ? (
                <text>{queryForm.beginTime}</text>
              ) : (
                <text class="placeholder">点击选择日期</text>
              )}
            </text>
          </tz-picker>
        </tz-cell>
        <tz-cell title="录单结束时间" border>
          <tz-picker v-model={queryForm.endTime} mode="date">
            {queryForm.endTime ? (
              <text>{queryForm.endTime}</text>
            ) : (
              <text class="placeholder">点击选择日期</text>
            )}
          </tz-picker>
        </tz-cell>
      </tz-form>
      <tz-button
        class="search-btn"
        type="primary"
        onClick={() => searchClick()}
      >
        搜索
      </tz-button>
    </tz-view>
  );
};
