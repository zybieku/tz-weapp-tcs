import api from "@/lib/tzAxios";

export const queryConsistPageApi = (data) => {
  const option = {
    url: "/wx/consistQueryApi/queryConsistPage",
    data,
  };
  return api.post(option);
};

// 删除确报
export const deleteConsistApi = (data) => {
  const option = {
    url: "/wx/consist/deleteConsist",
    data,
  };
  return api.post(option);
};
