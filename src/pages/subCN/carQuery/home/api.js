import api from "@/lib/tzAxios";

/**
 * 默认查询车辆集合  {currentPage: 1}
 */
export const carsQuery = (data) => {
    const option = {
        url: "/wx/carsApi/carsQuery",
        data: data,
    };
    return api.post(option);
};


/**
 * 删除  {id: "1c082ce1a279de5d2d00a111be1aeb89"}
 */
export const carsDel = (data) => {
    const option = {
        url: "/wx/carsApi/delSelfCar",
        data: data,
    };
    return api.post(option);
};

