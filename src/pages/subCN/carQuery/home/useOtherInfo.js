import Taro from "@tarojs/taro";

export const useTestnfo = (state) => {
  const radioList = [
    {
      value: 1,
      text: "是",
    },
    {
      value: 0,
      text: "否",
    },
  ];
  const handleClick = (e) => {
    console.log(e);
  };

  return () => (
    <view class="car-info">
      <tz-cell border title="内地车牌">
        <tz-input v-model={state.carNo} placeholder="请输入车牌"></tz-input>
      </tz-cell>
      <tz-cell border title="是否只读">
        <tz-switch type="checkbox" v-model={state.isCheck}></tz-switch>
        <tz-switch type="checkbox" v-model={state.isCheck}></tz-switch>
      </tz-cell>
      <tz-cell border title="是否切换">
        <tz-switch type="switch" v-model={state.isCheck}></tz-switch>
      </tz-cell>
      <tz-cell border title="是否radio切换">
        <tz-radio-group
          options={radioList}
          v-model={state.radioValue}
        ></tz-radio-group>
      </tz-cell>
    </view>
  );
};
