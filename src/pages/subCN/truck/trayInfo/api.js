import api from "@/lib/tzAxios";

/**
 * 托架查询
 */
export const queryTrailerApi = (data) => {
  const option = {
    url: "/wx/consistQueryApi/queryTrailer",
    data,
  };
  return api.post(option);
};
