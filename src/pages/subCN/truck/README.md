# 大陆捆绑

#### 作者：林梓钿

#### 时间：2021-10-15

## 需求背景

提供空车捆绑、搭载空箱捆绑、重车捆绑三个功能模块

## 项目文件和结构

truck  
├─addCar 新增/编辑车辆  
├─boxingInfo 编辑集装箱  
├─home 主页  
└─trayInfo 编辑托架

## 功能描述

无柜空车、有柜空车、重车捆绑进入的是同一个页面，业务区别在于：

- 个人账号：

  - 无柜空车：车辆类型为货柜车时，可以录入托架信息（非必填）；吨车则没有托架信息
  - 有柜空车：车辆类型只有货柜车，默认选中，选择车辆后可以录入托架信息和集装箱信息，托架信息和集装箱信息必填
  - 重车：选择货柜车时，展示托架信息和集装箱信息入口，托架信息必填，集装箱信息选填；选择吨车时，仅展示集装箱信息，没有托架信息，集装箱信息选填，没有填写不提示直接提交

- 司机账号：三种捆绑类型都不能选择车辆类型，添加车辆表单有车辆牌头公司字段，重车捆绑有【其他】字段

- 管理员账号：添加车辆表单有车辆牌头公司字段，重车捆绑有【其他】字段

#### 1.home 主页

主页总共分四个主要区域：【车辆信息】、【六联单号和口岸代码】、【托架信息】、【集装箱信息】。车辆信息是最主要的信息，进入主页有两种情况：1.从详情页过来编辑；2.新增数据。

1. 车辆信息  
   车辆信息 carInfo 初始状态为 null，从详情页过来带车辆数据，从新增车辆页面保存返回也会返回车辆数据。

2. 六联单号和口岸代码  
   固定显示的表单项，从详情页过来自动填充数据；如果是公司司机类型，重车捆绑有【其他】输入框。

3. 托架信息  
   托架信息展示规则：carInfo 车辆数据不为 null && 车辆类型不为吨车

4. 集装箱信息  
   集装箱信息展示规则：carInfo 车辆数据不为 null && 车辆类型不为无柜空车

5. 保存按钮  
   保存的时候通过 route.query.consistId 判断是否从详情页过来，再选择不同的跳转方式。

   - 保存时封志信息的处理：在重车类型添加的时候有封志信息，封志信息有三个字段【封志号】【封志类型】【施封人】，但在保存的时候需要将三个字段以"【封志类型】/【封志号】@【施封人】"的格式放在 boxingSealed 字段传给后端（注意：这个字段与编辑集装箱信息表单里面的封志号的字段名相同）

#### 2.addCar 新增/编辑车辆

在这个页面中可以新增车辆数据或者选择已有的车辆数据进行编辑，保存后回到大陆捆绑主页，展示已选的车辆信息。

- 进入页面后查询基础数据车辆列表，默认展示 9 条车辆数据，当车辆数据大于等于 9 条时展示搜索按钮查看更多，点击展示搜索框。

- 选择已存在车辆数据会将车辆数据处理后填充到表单。

- 如果路由参数中有车辆数据 carInfo，则对车辆数据进行处理后回填到表单中。

- 处理车辆数据统一使用方法 handleCarInfo。

- 处理车辆数据的时候如果存在集装箱数据 boxing 字段，将集装箱数据中的封志数据保存在 cacheSealedInfo 字段带回到 home 页面。

- 新增车辆按钮点击后跳转当前页，传参 type 为 'add' 用于识别当前页面为新增。

- 车辆的基础数据中如果有维护多个司机，司机姓名项为选择框，选中司机后自动填充司机的数据

#### 3.trayInfo 编辑托架

新增或编辑托架信息，保存将托架信息带回大陆捆绑主页

托架信息表单：

- 托架号  
  限制 6 位字母和数字；如果输入小写字母自动转成大写；非个人类型账号输入位数大于 3 的时候通过托架号查询托架信息，自动填充表单。

#### 4.boxingInfo 编辑集装箱

新增或编辑集装箱信息，保存将集装箱信息带回大陆捆绑主页

- 重车类型才有封志号信息，其他类型没有

- 业务需求点（禅道任务 4934）：重车确报录入集装箱调取复选框

  1. 重车确报录入集装箱时，如果当前车牌维护了封志信息（3 项），显示复选框，未维护不显示；
  2. 勾选复选框后，自动填写 3 项封志信息（封志号、封志类型、施封人）；取消勾选后自动清空 3 项信息

- 调取复选框的展示逻辑，在选择车辆后，如果数据中存在封志信息，就把封志信息存在车辆数据中的 cacheSealedInfo 属性，同时如果是重新选择车辆的情况下，把封志号三个字段删除掉，因为业务需求默认封志信息为空。点击调取后再将封志信息填充到表单。

```js
// pages/subCN/truck/addCar/useCarInfo.js

// 有维护封志信息才保存到缓存中，否则清空缓存的封志信息
if (boxingSealed || boxingSealedName || boxingSealedType) {
  state.carForm.data.cacheSealedInfo = {
    boxingSealed,
    boxingSealedName,
    boxingSealedType,
  };
  // 非编辑已选车辆情况下，清除封志信息字段，否则会展示和自动填充表单
  if (from === "isNew") {
    delete state.carForm.data.boxing[0].boxingSealed;
    delete state.carForm.data.boxing[0].boxingSealedName;
    delete state.carForm.data.boxing[0].boxingSealedType;
  }
}
```

- 业务需求点（禅道任务 4977）：吨车时集装箱信息填充默认值  
  集装箱号根据车辆信息自动带出海关编码、尺寸默认“22G1”、来源“第三方提供箱”、重箱/空箱默认“重箱”、自重为 0

```js
// pages/subCN/truck/boxingInfo/index.vue

// 重车吨车时，集装箱信息填写默认值
if (state.truckType.isTon && state.truckPayloadType.isHeavyCar) {
  if (route.query.truckLicenseCode) {
    state.formModel.boxingCode = route.query.truckLicenseCode;
  }
  // 自重
  state.formModel.boxingWeight = "0";
  // 来源 默认 '第三方提供箱'
  state.formModel.boxingSourceCode = "5";
  // 重箱/空箱 默认 '重箱'
  state.formModel.fullOrEmptyCode = "5";
  // 集装箱尺寸默认'22G1'
  state.formModel.boxingSizeType = "22G1";
}
```

## 注意事项

- truckPayloadType 用于判断进入大陆捆绑的车辆类型

  - 无柜空车捆绑 isNoBoxEmptyCar: truckOrderType === "1"
  - 有柜空车捆绑 isHasBoxEmptyCar: truckOrderType === "2"
  - 重车捆绑 isHeavyCar: truckOrderType === "3"

- truckType 为已选择的车辆数据类型
  - 货柜车 isContainer: truckType === "1"
  - 吨车 isTon: truckType === "0"

## 待优化点

- 选择了车辆之后，点击车辆信息返回编辑页面，数据的回显是通过路由带回去的，路由参数较长需要确认是否会有影响。

- 大陆捆绑首页，托架和集装箱信息的点击区域调整大一点

- 车辆选择和新增车辆完成返回捆绑首页，接收数据的代码很杂，需要优化
