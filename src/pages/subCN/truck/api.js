import api from "@/lib/tzAxios";

/**
 * 默认查询车辆集合
 */
export const carsQuery = (data) => {
  const option = {
    url: "/wx/carsApi/carsQuery",
    data,
  };
  return api.post(option);
};
/**
 * 根据条件查询车辆集合
 * param {obj} data
 */
export const listByTruckName = (data) => {
  const option = {
    url: "/wx/carsApi/listByTruckName",
    data: data,
  };
  return api.post(option);
};
/**
 * 根据id条件查询车辆信息
 * param {string} data
 */
export const carsQueryById = (data) => {
  const option = {
    url: "/wx/carsApi/carsQueryById",
    data: data,
  };
  return api.post(option);
};
/**
 * 编辑车辆信息
 * param {obj} data
 */
export const saveSelfCar = (data) => {
  const option = {
    url: "/wx/carsApi/saveSelfCar",
    data: data,
  };
  return api.post(option);
};
/**
 * 保存确报url
 * @param {obj} data
 */
export const saveConsist = (data) => {
  const option = {
    url: "/wx/consist/saveConsist",
    data: data,
  };
  return api.post(option);
};
