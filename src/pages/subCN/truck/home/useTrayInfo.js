import Taro from "@tarojs/taro";
// import { reactive, ref, computed } from "vue";
// import { carsQuery, listByTruckName, carsQueryById, saveSelfCar } from "../api";

export const usetrayInfo = ({ state, dicMap, navigateTo, coldCompany }) => {
  const trayData = [
    { label: "托架号", value: "trailerCode" },
    { label: "托架规格", value: "trailerTypeCode" },
    { label: "架重(KG)", value: "trailerWeight" },
  ];
  // 处理托架规格显示中文
  const handletrailerTypeCode = (val) => {
    const etrailerTypeName = dicMap.trailerTypeCodeList.find((item) => {
      return item.para_value === val && item.para_name;
    });

    if (etrailerTypeName) {
      return etrailerTypeName.para_name;
    } else {
      return "";
    }
  };

  coldCompany.isColdCompany === "01" &&
    trayData.push({ label: "发电机重量(KG)", value: "generatorWeight" });

  const trayDataList = () => {
    if (state.trayInfo) {
      return trayData.map((item) => (
        <tz-view>
          <text>{item.label}</text>
          {item.value === "trailerTypeCode"
            ? handletrailerTypeCode(state.trayInfo[item.value])
            : state.trayInfo[item.value]}
        </tz-view>
      ));
    }
  };

  const handleAddTray = () => {
    navigateTo({
      path: "/pages/subCN/truck/trayInfo/index",
    });
  };
  const handleDetail = () => {
    console.log(state.trayInfo);
    navigateTo({
      path: "/pages/subCN/truck/trayInfo/index",
      query: { trayInfo: JSON.stringify(state.trayInfo) },
    });
  };
  const handleDel = () => {
    Taro.showModal({
      content: "确定删除该条记录吗?",
      confirmText: "确定",
      cancelText: "取消",
    }).then((res) => {
      res.confirm && (state.trayInfo = null);
    });
  };

  return () => (
    <>
      {/* state.truckType isTon 吨车  isContainer 货柜车 */}
      <tz-view
        class="title-content  margin-top20"
        v-show={state.carInfo !== null && !state.truckType.isTon}
      >
        <text>托架信息</text>
        <tz-button
          type="primary"
          size="small"
          onClick={handleAddTray}
          v-show={state.trayInfo === null}
        >
          添加托架
        </tz-button>

        {
          <tz-view
            class="cell-info"
            v-show={
              state.trayInfo && state.trayInfo.hasOwnProperty("trailerCode")
            }
          >
            <tz-view class="info-row">
              <tz-view class="tray-data" onClick={() => handleDetail()}>
                {trayDataList()}
              </tz-view>
              <tz-view class="del-box" onClick={handleDel}>
                <tz-icon name="close"></tz-icon>
                删除
              </tz-view>
            </tz-view>
          </tz-view>
        }
      </tz-view>
    </>
  );
};
