import { getDicMap } from "@/utils/storage";

export const useCarInfo = ({ state, navigateTo, dicMap }) => {
  const { countryCode } = dicMap;
  const carInfoConfig = [
    { label: "内地车牌", value: "truckLicenseName" },
    { label: "车辆海关编码", value: "truckLicenseCode" },
    {
      label: "车辆类型",
      value: () => {
        return (
          { 0: "吨车", 1: "货柜车" }[state.carInfo.truckType] ||
          state.carInfo.truckType
        );
      },
    },
    { label: "司机姓名", value: "driverName" },
    { label: "司机卡号", value: "driverCode" },
    { label: "司机联系方式", value: "dMobile" },
  ];
  if (!getDicMap().userType.isPersonal) {
    carInfoConfig.push({ label: "车辆牌头公司", value: "belongName" });
  }
  carInfoConfig.push(
    ...[
      { label: "司乘人员数", value: "numStaffMember" },
      {
        label: "健康状态",
        value: () => {
          return (
            { 0: "异常", 1: "正常" }[state.carInfo.healthStatus] ||
            state.carInfo.healthStatus
          );
        },
      },
      {
        label: "有预防接种",
        value: () => {
          return (
            { 0: "否", 1: "是" }[state.carInfo.vaccinate] ||
            state.carInfo.vaccinate
          );
        },
      },
      {
        label: "途径国家(地区)",
        value: () => countryCode[state.carInfo.rountingCountryCode],
      },
    ]
  );

  const renderCarInfo = (config) => {
    return config.map((item) => {
      if (item.children) {
        return (
          <tz-view class="children-flex">
            {renderCarInfo(item.children)}
          </tz-view>
        );
      } else {
        return (
          <tz-view>
            <text>{item.label}</text>
            {item.value instanceof Function
              ? item.value()
              : state.carInfo[item.value]}
          </tz-view>
        );
      }
    });
  };
  const carClick = (isEdit) => {
    const navParams = {
      path: "/pages/subCN/truck/addCar/index",
      query: {
        truckPayloadType: JSON.stringify(state.truckPayloadType),
        detailToEdit: state.detailToEdit,
        truckOrderType: state.truckOrderType, // 捆绑类型 1 无柜空车；2 有柜空车；3 重车
      },
    };
    /**
     * 这里把已经添加的车辆数据传到编辑车辆页面
     * isEdit 进入编辑车辆数据
     */
    if (state.carInfo && isEdit) {
      Object.assign(navParams.query, {
        carInfo: JSON.stringify(state.carInfo),
        type: "edit",
      });
    }
    navigateTo(navParams);
  };

  return () => (
    <>
      <tz-view class="title-content">
        <text>车辆信息</text>
        {!state.detailToEdit && (
          <tz-button type="primary" size="small" onClick={() => carClick()}>
            车辆选择
          </tz-button>
        )}

        {state.carInfo && (
          <tz-view class="car-info" onClick={() => carClick(true)}>
            <tz-view class="car-info-content">
              {renderCarInfo(carInfoConfig)}
            </tz-view>
            <tz-icon name="arrow-right"></tz-icon>
          </tz-view>
        )}
      </tz-view>
    </>
  );
};
