import { getConsistBeanByIdApi } from "./api";
import { getDicMap } from "@/utils/storage";

export const useMountData = ({ state, consistId }) => {
  const { customsCodeList } = getDicMap();
  getConsistBeanByIdApi({ consistId }).then((res) => {
    const consistBean = res.data.consistBean;
    state.others = consistBean.others || "";

    // 口岸代码
    const vrn = consistBean.truckList[0].truckLicenseName;
    if (vrn.indexOf("澳") !== -1) {
      state.ports = [
        { para_value: "", para_name: "--请选择--" },
        { para_value: "5795", para_name: "5795横琴海关" },
        { para_value: "5710", para_name: "5710闸口海关" },
        { para_value: "5791", para_name: "5791拱跨工区" },
        { para_value: "5788", para_name: "5788大桥海关" },
      ];
    } else {
      state.ports = customsCodeList;
    }
    state.customsCode = consistBean.customsCode;
    // 六联单号
    state.docCode = consistBean.docCode;

    // 货物通关代码 TODO: 此处旧代码中仅取值，表单中没有此项，后续根据现有需求调整
    state.goodsCustomsCode = consistBean.goodsCustomsCode;

    if (consistBean.truckList && consistBean.truckList.length > 0) {
      // 车辆信息
      const carInfo = consistBean.truckList[0];
      carInfo.truckId = consistBean.truckList[0].truckId;
      state.carInfo = carInfo;

      // 增加车辆类型的识别对象
      if (!state.truckType) {
        state.truckType = {
          isContainer: carInfo.truckType === "1", // 货柜车
          isTon: carInfo.truckType === "0", // 吨车
        };
      }

      // 托架信息
      if (
        carInfo.trailerCode ||
        carInfo.trailerTypeCode ||
        carInfo.trailerWeight
      ) {
        const {
          trailerCode,
          trailerTypeCode,
          trailerWeight,
          isEnable,
          generatorWeight,
        } = carInfo;
        state.trayInfo = {
          trailerCode,
          trailerTypeCode,
          trailerWeight,
          isEnable,
          generatorWeight,
        };
      }
      // 集装箱信息
      if (carInfo.truckBoxings) {
        const truckBoxings = carInfo.truckBoxings;
        // 封志号相关
        if (state.truckPayloadType.isHeavyCar && truckBoxings.length) {
          truckBoxings.forEach((item) => {
            if (item.boxingSealed) {
              const splitSealed = item.boxingSealed.split("/");
              // 此处保留原有逻辑
              if (splitSealed.length > 0) {
                // 封志类型
                item.boxingSealedType = splitSealed[0];
              }
              if (splitSealed.length > 1) {
                const secondSplit = splitSealed[1].split("@");
                if (secondSplit.length > 0) {
                  // 封志号
                  item.boxingSealed = secondSplit[0];
                }
                if (secondSplit.length > 1) {
                  // 施封人
                  item.boxingSealedName = secondSplit[1];
                }
              }
            }
          });
        }

        /**
         * NOTE: 业务需求
         * 默认不带封志信息，将封志信息缓存起来
         * 存在封装信息时，集装箱表单页面展示调取选择框
         * 勾选调取，自动填充缓存的封志信息
         */
        /**
         * NOTE:
         * 处理封志信息
         * 旧版是把封志信息存在本地缓存，缓存在本地有个问题，
         * 正常情况调取的封志号信息是基础数据里面维护的，
         * 切换车辆，维护的封志信息也会不一样
         * 如果存在storage里面，在切换车辆时封志信息更新缓存，不保存车辆
         * 这时候原有的车辆的封志信息调取就不正确了
         */
        // 
        // 
        const {
          boxingSealed,
          boxingSealedName,
          boxingSealedType,
        } = carInfo.truckBoxings[0];
        // 有维护封志信息才保存到缓存中，否则清空缓存的封志信息
        if (boxingSealed || boxingSealedName || boxingSealedType) {
          state.carInfo.cacheSealedInfo = {
            boxingSealed,
            boxingSealedName,
            boxingSealedType,
          };
        }

        state.boxingInfo = carInfo.truckBoxings;
        // REVIEW: 兼容旧字段 boxing
        state.carInfo.boxing = carInfo.truckBoxings;
      }
    }
    state.cacheEditFormData = consistBean; // 缓存捆绑数据
  });
};
