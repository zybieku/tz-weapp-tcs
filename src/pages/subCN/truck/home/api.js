import api from "@/lib/tzAxios";

/**
 * 确报详情
 */
export const getConsistBeanByIdApi = (data) => {
  const option = {
    url: "/wx/consistQueryApi/getConsistBeanById",
    data,
  };
  return api.post(option);
};
