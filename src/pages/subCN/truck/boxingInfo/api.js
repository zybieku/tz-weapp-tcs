import api from "@/lib/tzAxios";

/**
 * 确报系统设置
 */
export const querySettingApi = (data) => {
  const option = {
    url: "/wx/consist/querySetting",
    data,
  };
  return api.post(option);
};

//集装箱查询
export const queryBoxingApi = (data) => {
  const option = {
    url: "/wx/consistQueryApi/queryBoxing",
    data,
  };
  return api.post(option);
};
